const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');
const port = 8080;
const app = express();
require('dotenv').config();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const db_env = {
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
}

console.log("db_environment =", db_env);

const db = mysql.createPool({
  connectionLimit: 10,
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
});

db.getConnection((err, connection) => {
  if (err) {
    console.error('Error connecting to MySQL:', err.message);
    return;
  }

  console.log('Connected to Mysql');
  connection.release(); 
});

db.on('error', (err) => {
  console.error('MySQL pool error:', err.message);
});

app.get("/products", async (req, res) => {
  db.query('SELECT * FROM products', (error, results) => {
    if (error) throw error;
    else if (results.length ===0){
      res.status(404).send({
        "status": "not_found",
        "product": results
      });
    } else {
      res.status(200).send({
        "status": "ok",
        "product": results
      });
    }
  });
});

app.get("/products/:id", async (req, res) => {
  const id = parseInt(req.params.id);
  db.query(`SELECT * FROM products WHERE product_id = '${id}'`, (error, results) => {
    if (error) throw error;
    else if (results.length ===0){
      res.status(404).send({
        "status": "not_found",
        "product": results
      });
    } else {
      res.status(200).send({
        "status": "ok",
        "product": results
      });
    }
  })
});

app.post("/products", (req, res) => {
  let product = req.body;
  if(!product.price){
      return res.status(400).send("Please fill product price");
  } else if(isNaN(parseInt(product.price))){
      return res.status(400).send("Price must be number");
  } 
  if(!product.stock){
      return res.status(400).send("Please fill product stock");
  } else if(isNaN(parseInt(product.stock))){
      return res.status(400).send("Stock must be number");
  } 
  if(!product.name){
      return res.status(400).send("Please fill product name");
  }
  if(!product.category){
      return res.status(400).send("Please fill product category");
  }
  db.query(`INSERT INTO products(name, price, stock, category) VALUES("${product.name}", "${product.price}", "${product.stock}", "${product.category}")`, (error, results) => {
    if (error) throw error;
    else {
      res.status(201).send({
        "status": "ok",
        "message": "Product is created",
        "product": product
      });
    }
  })
});

app.put("/products", (req, res) => {
  const product = req.body;
  const id = parseInt(product.id);

  db.query(`SELECT * FROM products WHERE product_id = '${id}'`, (error, results) => {
    if (error) throw error;
    else if (results.length ===0){
      res.status(404).send({
        "status": "not_found",
        "product": results
      });
    } else {
      db.query(`UPDATE products SET name='${product.name}', price='${product.price}', stock='${product.stock}', category='${product.category}' WHERE product_id = ${id};`, (error, results) => {
        if (error) throw error;
        else {
          res.status(200).send({
            "status": "ok",
            "message": "Product ID "+id+"is updated",
            "product": product
          });
        }
      })
    }
  })
});

app.delete("/products/:id", (req, res) => {
  const id = parseInt(req.params.id);
  db.query(`SELECT * FROM products WHERE product_id = '${id}'`, (error, results) => {
    if (error) throw error;
    else if (results.length ===0){
      res.status(404).send({
        "status": "not_found",
        "product": results
      });
    } else {
      db.query(`DELETE FROM products WHERE product_id = ${id}`, (error, results) => {
        if (error) throw error;
        else {
          res.status(200).send({
            "status": "ok",
            "message": "Product with ID = "+id+" is deleted"
          });
        }
      })
    }
  })
});

app.listen(port, () => {
  console.log(`server is running on port ${port}.`)
})