CREATE TABLE products (
    product_id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    price INT NOT NULL,
    stock INT,
    category VARCHAR(128)
);

INSERT INTO products(name, price, stock, category) VALUES("airpod", "5000", "3", "device");

